/**
    * @file "HostMessage.java"
*/

/**
    * @brief class for storing a parsed command sent from the host
*/
public class HostMessage {
    public COMMAND command;             // type of command
    public int arg1, arg2, arg3, arg4;  // integer arguments
    public String arg5;                 // string argument
}
