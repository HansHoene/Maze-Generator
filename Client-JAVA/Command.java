/**
    * @file "Command.java"
    * @brief contains COMMAND enumeration
*/

/**
    * @brief enumeration for types of Inter-Process Communication (IPC) commands
*/
public enum COMMAND {
    
    /*
    EXACT COPY FROM C++ code
    */

    // from host to client
    OVER,
    CHANGE,
    SEND_MESSAGE,
    DIM,

    // from client to host
    NEW,
    REFRESH,
    MOVE,
    TOGGLE_TRACE,

    // can be sent be either one
    EXIT,
    NOTHING;
    
    /**
        * @brief resolves command to integer
        * @param[in] "COMMAND val" enumeration value
        * @return command as integer
    */
    public static int toInt(COMMAND val) {
        switch(val) {
            case OVER:
                return 0;
            
            case CHANGE:
                return 1;
                
            case SEND_MESSAGE:
                return 2;
                
            case DIM:
                return 3;
                
            case NEW:
                return 4;
                
            case REFRESH:
                return 5;
                
            case MOVE:
                return 6;
                
            case TOGGLE_TRACE:
                return 7;
                
            case EXIT:
                return 8;
                
            default:
                return 9;   // nothing
        }
    }
    
    /**
        * @brief resolves integer to command
        * @param[in] "int val" integer value
        * @return integer as command
    */
    public static COMMAND valueOf(int val) {
        switch(val) {
            case 0:
                return OVER;
                
            case 1:
                return CHANGE;
                
            case 2:
                return SEND_MESSAGE;
                
            case 3:
                return DIM;
                
            case 4:
                return NEW;
                
            case 5:
                return REFRESH;
                
            case 6:
                return MOVE;
                
            case 7:
                return TOGGLE_TRACE;
                
            case 8:
                return EXIT;
                
            default:
                return NOTHING;     // 9
        }
    }

}
