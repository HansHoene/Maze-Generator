/**
    * @file "Numberfield.java"
*/

import java.awt.TextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
    * @brief a TextField that only accepts integer input
*/
public class NumberField extends TextField implements KeyListener {

	private static final long serialVersionUID = 1L;
	
	/**
	    * @brief construction
	    * @param[in] "String txt" initial value
	*/
	public NumberField(String txt) {
		super();
		super.addKeyListener(this);
		setText(txt);
	}

	/**
	    * @brief construction
	*/
	public NumberField() {
		this("");
	}
	
	/**
	    * @brief handles key types
	    * @param[in] "KeyEvent event" key type event passed by JVM or OS
	    * 
	    * Cancels key presses that are not integers, and handles special key presses.
	*/
	public void keyTyped(KeyEvent event) {
		
		char c;								// character that was typed
		int code;                           // key code
		
		c = event.getKeyChar();
		code = event.getKeyCode();
		
		if (event.getSource() == this) {	// if this the source of the event?
			
			if (c < '0' || c > '9') {		// not a number?
				// cancel the event and handle special characters
				
				event.consume();
				
				/* handle special characters */
				if (c == '-') {				// is negative sign?
					// toggle sign
					
					if (super.getText().charAt(0) == '-') {	        // is field already negative?
						super.setText(super.getText().substring(1));
					} else {
						super.setText('-' + super.getText());
					}
					
				} else if (c == '\b') {
				    super.setText("");
				}
				
				/* move cursor to back */
				super.setCaretPosition(super.getText().length());
				
			}
			
		}
	}
	
	/**
	    * @brief sets text value of NumberField
	    * @param[in] "String txt" integer value as string
	*/
	public void setText(String txt) {
	    int i;
	    char c;
	    super.setText("");
	    
	    for (i = 0; i < txt.length(); i++) {
	        c = txt.charAt(i);
	        
	        if (c < '0' || c > '9') {      // not a number?
                // cancel the event and handle special characters
                
                /* handle special characters */
                if (c == '-') {             // is negative sign?
                    // toggle sign
                    
                    if (super.getText().charAt(0) == '-') {         // is field already negative?
                        super.setText(super.getText().substring(1));
                    } else {
                        super.setText('-' + super.getText());
                    }
                    
                } else if (c == '\b') {
                    super.setText("");
                }
                
                /* move cursor to back */
                super.setCaretPosition(super.getText().length());
                
            } else {
                super.setText(super.getText() + c);
                super.setCaretPosition(super.getText().length());
            }
	    }
	}
	
	/**
	    * @return integer value of NumberField
	*/
	public int getIntValue() {
	    return Integer.parseInt(super.getText());
	}

	/* Interface functions that are not needed */
	public void keyPressed(KeyEvent arg0) {}
	public void keyReleased(KeyEvent arg0) {}
	
}   // end of class
