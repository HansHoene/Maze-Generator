/**
    * @file "Client.java"
    * @brief the main user interface
*/

import java.awt.*;
import java.awt.event.*;

import javax.swing.JOptionPane;

/**
    * @brief implements the main user interface and uses lower-level classes for Inter-Process Communication w/ host
    * 
    * Most of the options in this class are private.  The only method needed is
    * the "public static void playGame(int port)".  This method uses the private
    * constructor to create a user interface.  The user interface will create
    * references to the modules needed to sustain a maze game.  An IPC object is
    * created for Inter-Process Communication with the host.  A Grid object is
    * used to show maze data from the host onto a user-friendly GUI.  This user
    * interface is simple and it gives easy access to user options.  This class
    * implements several listeners, which are used for handling events from
    * various components.
    * 
    * @see IPC.java
    * @see Grid.java
*/
public class Client implements KeyListener, MouseListener, WindowListener, ComponentListener {
    
    /* constants */
    private final static Font 
        font = new Font("Ariel", 0, 30);// font used in various GUI components
    
    /* GUI Components */
    private Frame window;               // main window
    private Button newMaze;             // button to request new maze
    private Button toggleTrace;         // button to toggle trace option
    private Label rowInputLabel,
                  colInputLabel;        // labels for row and column input boxes
    private NumberField rowInput,
                        colInput;       // input boxes for rows and columns
                                        // when requesting a new maze, these
                                        // input boxes are used to request a
                                        // particular maze size
    
    /* modules */
    private Grid maze;                  // GUI component used to dynamically draw a maze
    private IPC ipc;                    // implements Inter-Process Communication (IPC) protocol
    
    
    /* other tracking variables */
    boolean isOver;                     // is the game over?
                                        // used to prevent user from making moves after game has ended
                                        // when true, keyboard events do not trigger requests to move
    
    /**
        * @brief start a new game
        * @param[in] "int port" the port in which the host has a server socket binded to
        * 
        * This function initialises a Client object, then it starts a thread
        * which plays the game.
    */
    public static void playGame(int port) throws java.io.FileNotFoundException, java.io.IOException {
        Client obj;
        
        obj = new Client(port);
        
        (new Thread() {
            public void run() {
                try {
                    obj.playGame();
                } catch (java.io.IOException err) {}
            }
        }).start();
    }
    
    /**
        * @brief private constructor for Client responsible for setting up the game
        * @param[in] "int port" the port in which the host has a server socket binded to
        * @see IPC.java
        * 
        * Creates the GUI and initialises IPC via an IPC object
    */
    private Client(int port) throws java.io.IOException {
        
        /* initialise IPC */
        ipc = new IPC(port);
        
        /* create main window */
        window = new Frame("Maze Game");            // initialise
        window.setLocation(10, 10);                 // location
        window.setSize(1400, 1000);                 // size
        window.setLayout(null);                     // layout
        window.addWindowListener(this);
        window.addKeyListener(this);                // add listeners
        
        /* create maze */
        maze = new Grid(2, 2);                      // create 2x2 maze -- host command will change 
                                                    // dimensions before user even sees it
        maze.setLocation(20, 60);
        maze.setSize(900, 900);
        maze.addKeyListener(this);
        window.add(maze);
        
        /* create toggle trace button */
        toggleTrace = new Button("Toggle Trace");
        toggleTrace.setLocation(980, 100);
        toggleTrace.setSize(300, 50);
        toggleTrace.setFont(font);
        toggleTrace.addKeyListener(this);           // for making keyboard arrow moves on maze even
                                                    // if this button is being focused on
        toggleTrace.addMouseListener(this);         // for responding to button clicks
        window.add(toggleTrace);
        
        /* create row input */
        rowInputLabel = new Label("# of Rows:");    // label for row input
        rowInput = new NumberField();               // # of rows input
        rowInputLabel.setLocation(980, 200);
        rowInput.setLocation(1180, 200);
        rowInputLabel.setSize(200, 50);
        rowInput.setSize(100, 50);
        rowInputLabel.setFont(font);
        rowInput.setFont(font);
        rowInputLabel.setFocusable(true);           // if you focus on text, you can still send
                                                    // keyboard events because of this
        rowInput.setText("10");                     // default value for row input
        rowInputLabel.addKeyListener(this);
        rowInput.addKeyListener(this);              // can still send keyboard arrow moves when 
                                                    // this is focused on
        window.add(rowInputLabel);
        window.add(rowInput);
        
        /* create column input */
        colInputLabel = new Label("# of Columns:"); // label for col input
        colInput = new NumberField();               // # of cols input
        colInputLabel.setLocation(980, 250);
        colInput.setLocation(1180, 250);
        colInputLabel.setSize(200, 50);
        colInput.setSize(100, 50);
        colInputLabel.setFont(font);
        colInput.setFont(font);
        colInputLabel.setFocusable(true);           // if you focus on text, you can still send
                                                    // keyboard events because of this
        colInput.setText("10");                     // default value for col input
        colInputLabel.addKeyListener(this);
        colInput.addKeyListener(this);              // can still send keyboard arcol moves when 
                                                    // this is focused on
        window.add(colInputLabel);
        window.add(colInput);
        
        /* create new maze button */
        newMaze = new Button("New Maze");
        newMaze.setLocation(980, 300);
        newMaze.setSize(300, 50);
        newMaze.setFont(font);
        newMaze.addKeyListener(this);               // send keyboard events if this is focus
        newMaze.addMouseListener(this);             // respond to clicks
        window.add(newMaze);

        /* launch window */
        isOver = false;                             // game is not over
        newMaze(10, 10);                            // request new maze of size 10
        window.setVisible(true);                    // set main window visible
    }
    
    /**
        * @brief draw a new maze
        * @param[in] "int rows" # of rows to draw
        * @param[in] "int cols" # of columns to draw
        * 
        * The main thread function, playGame, calls this function when the host
        * asks the Client to change dimensions.  Whenever such a request is sent
        * from the host, a new maze shall be drawn.  Asking to refresh the maze
        * (via IPC) is unnecessary because the host will already be sending it
        * in future requests.  This function must only clear the board and set
        * it up again to prepare for those future requests.
    */
    private void newMaze(int rows, int cols) {
        isOver = false;                         // new game is not over
                                                // enable keyboard listener to send requests
        maze.changeDimensions(rows, cols);      // clear maze and change dimensions
        ipc.refreshBoard();                     // this seems redundant, but on some systems, 
                                                // if you do not refresh, (0, 0) will not be 
                                                // marked as selected for some unknown reason
    }
    
    /**
        * @brief read host requests and respond properly over IPC
        * @see IPC.java
        * 
        * This function is called after the constructor in order to play a game.
        * This function has the responsibility of parsing request after request
        * via IPC and updating the GUI accordingly.
    */
    private void playGame() throws java.io.IOException {
        
        HostMessage request;
        
        while (true) {                          // infinite loop -- do not stop until quit request is sent
            
            request = ipc.getCommand();         // read a request
            
            switch (request.command) {          // parse request command
                
                // IPC handles all error checking other than logical error checking
                // the values are there, you just need to make sure those values are valid
                
                case OVER:
                    // user finished maze
                    JOptionPane.showMessageDialog(window, "You got to the end!");
                    isOver = true;
                    break;
                    
                case CHANGE:
                    // status of Cell has changed
                    
                    /* logical error check  */
                    if (request.arg1 >= 0 && request.arg2 >= 0      // x or y tbig enough?
                     && request.arg3 >= 0 && request.arg3 <= 3      // good CellStatus?
                     && request.arg1 < maze.getNumRows()            // x small enough?
                     && request.arg2 < maze.getNumCols()) {         // y small enough?
                        
                        /* have maze re-draw Cell with updated values */
                        maze.updateCell(request.arg2, request.arg1, CellStatus.valueOf(request.arg3),
                                ((request.arg4 >> 3) & 0x1) == 1,
                                ((request.arg4 >> 2) & 0x1) == 1,
                                ((request.arg4 >> 1) & 0x1) == 1,
                                ((request.arg4 >> 0) & 0x1) == 1
                        );
                        
                    }
                    break;
                    
                case SEND_MESSAGE:
                    /* alert user */
                    JOptionPane.showMessageDialog(window, request.arg5);
                    break;
                    
                case DIM:
                    /* change dimensions of maze because new maze will be sent */
                    newMaze(request.arg1, request.arg2);
                    break;
                    
                case EXIT:
                    /* host is aborting program if this message is sent */
                    /*
                    NOTE: as far as I know, I have not programmed any scenarios
                    where abort message is sent from host, but it is good to
                    have this programmed anyways.
                    */
                    
                    ipc.close();
                    JOptionPane.showMessageDialog(window, "Error: host program has unexpectedly exited.");
                    window.dispose();
                    System.exit(0);
                    break;
                    
                default:
                    // otherwise, do nothing
                    break;
                
            }   // end of switch parsing statement
        }       // end of infinite loop
    }           // end of function
    
    /**
        * @brief handles all key presses in window
        * @param[in] "KeyEvent e" parameter passed from the JVM or OS
        * @see IPC.java
        * 
        * If the current game is not over and an arrow key is pressed, use IPC
        * to send a move request to the host.
    */
    public void keyPressed(KeyEvent e) {
        /* move */
        if (!isOver) {                          // is the game not over?
            
            switch(e.getKeyCode()) {            // code of key pressed -- which direction?
                
                case KeyEvent.VK_UP:
                    // up arrow key
                    ipc.move(Direction.UP);
                    break;
                    
                case KeyEvent.VK_DOWN:
                    // down arrow key
                    ipc.move(Direction.DOWN);
                    break;
                    
                case KeyEvent.VK_LEFT:
                    // left arrow key
                    ipc.move(Direction.LEFT);
                    break;
                    
                case KeyEvent.VK_RIGHT:
                    // right arrow key
                    ipc.move(Direction.RIGHT);
                    break;
                
                default:
                    // not an arrow key -- do not send move command
                    break;
                    
             }  // end of switch-statement
        }       // end of if-statement
    }           // end of function
    
    /**
        * @brief handles all mouse clicks in window
        * @param[in] "MouseEvent e" parameter passed from the JVM or OS
        * @see IPC.java
        * 
        * Handle mouse clicks for buttons in Frame.  Use IPC to send requests to
        * host based on source of click.
    */
    public void mouseClicked(MouseEvent e) {
        
        if (e.getSource() == newMaze) {             // is user requesting a new maze?
            
            ipc.newBoard(rowInput.getIntValue(), colInput.getIntValue());
            
        } else if (e.getSource() == toggleTrace) {  // is user requesting to toggle trace?
            
            ipc.toggleTrace();
            
        }
    }
    /**
        * @brief called when user closes window -- exits the program
        * @param[in] "WindowEvent e" parameter passed from the JVM or OS
    */
    public void windowClosing(WindowEvent e) {
        ipc.close();            // tell host to exit
        window.dispose();       // dispose of GUI
        System.exit(0);         // exit JVM
    }
    
    /**
        * @brief called when user focuses on this window
        * @param[in] "WindowEvent e" parameter passed from the JVM or OS
        * @see Grid.java
        * 
        * Note: in response to a bug where the Grid (maze) unexpectedly
        * repaints, it is necessary to refresh the maze by re-sending all of the
        * data whenever the window is focused on.  Before adding this function,
        * the Grid always cleared when the window was minimised.  Since there is
        * no refresh button for the user, the data was only recoverable one Cell
        * at a time while blindly moving around.coverable.  Therefore, I added
        * this automation tool, which always refreshes the maze for the user in
        * the circumstances where it is possible that the maze may have been
        * lost.
    */
    public void windowActivated(WindowEvent e) {
        ipc.refreshBoard();
    }
    
    /*
    The following interface functions are from implemented interfaces, but these
    particular functions are not needed.  They are necessary for compilation
    though.
    */
    
    public void keyTyped(KeyEvent e) {}
    public void keyReleased(KeyEvent e) {}
    
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
    public void mousePressed(MouseEvent e) {}
    public void mouseReleased(MouseEvent e) {}
    
    public void windowDeactivated(WindowEvent e) {}
    public void windowClosed(WindowEvent e) {}
    public void windowDeiconified(WindowEvent e) {}
    public void windowIconified(WindowEvent e) {}
    public void windowOpened(WindowEvent e) {}
    
    public void componentHidden(ComponentEvent e) {}
    public void componentMoved(ComponentEvent e) {}
    public void componentResized(ComponentEvent e) {}
    public void componentShown(ComponentEvent e) {}
    
} // end of class
