/**
    * @file "Grid.java"
    * @brief contains methods of dynamically drawing maze
*/

import java.awt.*;
import javax.swing.*;

/**
    * @brief dynamically draws maze
    * 
    * Rather than keeping an array of actual Cells, the maze is drawn directly
    * from data sent by the host.  The client game function parses those
    * commands and passes arguments here.  When repainting, the canvas clears.
    * Once cleared, the updateCell function is used to colour and draw borders
    * for each Cell one at a time per command by the host.  The Grid must store
    * how many rows and columns are in the maze so that it can calculate the
    * pixel location of a Cell's cartesian coordinates.
*/
public class Grid extends Canvas {
    
    /* constants */
    private final static int padding = 2;           // white space width on each side of canvas
    private final static BasicStroke 
                lineStroke = new BasicStroke(3);    // stroke of brush -- determines border thickness
    
    private final static Color
                empty = Color.white,                // colour of an empty Cell
                selected = Color.green,             // colour of the selected Cell
                trace = Color.yellow,               // colour of a Cell along the trace path
                end = Color.pink,                   // colour of endpoint
                line = Color.black;                 // line/border colour
    
    /* tracking variables */
    private int dim;                                // pixels per Cell
    private int rows, cols;                         // Grid dimensions
    
    /**
        * @brief constructs a Grid of the given initial dimensions
        * @param[in] "int numRows" # of rows
        * @param[in] "int numCols" # of columns
    */
    public Grid(int numRows, int numCols) {
        super();
        changeDimensions(numRows, numCols);         // draw maze borders and set dimensions
    }
    
    /**
        * @brief get pixel location from row/col #
        * @param[in] "int rc" row or column number
        * @return upper-left Cell location as pixel number
        * 
        * This function can be used for rows and columns because the Grid Cells'
        * widths are equal to their heights.
    */
    private int getPixel(int rc) {
        return padding + (dim * rc);
    }
    
    /**
        * @brief change dimensions of maze to prepare for new maze
        * @param[in] "int numRows" # of rows
        * @param[in] "int numCols" # of cols
        * 
        * This functions changes tracking variables and it clears the board.
    */
    public void changeDimensions(int numRows, int numCols) {
        
        int verticalDim;
        int horizontalDim;
        
        /* change stored dimensions */
        rows = numRows;
        cols = numCols;
        
        /* get maximum dimension size for both dimensions -- vertical and horizontal */
        verticalDim = (super.getHeight() - (padding << 1)) / rows;
        horizontalDim = (super.getWidth() - (padding << 1)) / cols;
        
        /* use the smaller dimension so that maze Cells are squares */
        if (verticalDim < horizontalDim) {
            dim = verticalDim;
        } else {
            dim = horizontalDim;
        }
        
        /* clear canvas */
        repaint();
    }
    
    /**
        * @brief redraw a Cell by drawing in new borders a refillingwith new colour
        * @param[in] "int x" column #
        * @param[in] "int y" row #
        * @param[in] "CellStatus status" status of Cell (determines colour)
        * @param[in] "boolean up" is the cell connected to neighbour above?
        * @param[in] "boolean down" is the cell connected to neighbour below?
        * @param[in] "boolean left" is the cell connected to the neighbour on the left?
        * @param[in] "boolean right" is the cell connected to the neighbour on the right?
    */
    public void updateCell(int x, int y, CellStatus status, 
            boolean up, boolean down, boolean left, boolean right) {
        
        Graphics2D g;
        
        g = (Graphics2D)super.getGraphics();
        
        /* set border line stroke */
        g.setStroke(lineStroke);
        
        /* set appropriate fill colour */
        switch (status) {
            case EMPTY:
                g.setColor(empty);
                break;
            case SELECTED:
                g.setColor(selected);
                break;
            case TRACE:
                g.setColor(trace);
                break;
            default:
                g.setColor(end);
        }
        
        /* clear surrounding border with fill colour */
        g.drawLine(getPixel(x), getPixel(y), getPixel(x + 1), getPixel(y));             // upper border
        g.drawLine(getPixel(x), getPixel(y + 1), getPixel(x + 1), getPixel(y + 1));     // lower border
        g.drawLine(getPixel(x), getPixel(y), getPixel(x), getPixel(y + 1));             // left border
        g.drawLine(getPixel(x + 1), getPixel(y), getPixel(x + 1), getPixel(y + 1));     // right border
        
        /* fill rectangle in with fill colour */
        g.fillRect(getPixel(x), getPixel(y), 
                dim, dim);
        
        /* draw new borders */
        g.setColor(line);
        if (!up) {
            g.drawLine(getPixel(x), getPixel(y), getPixel(x + 1), getPixel(y));
        }
        if (!down) {
            g.drawLine(getPixel(x), getPixel(y + 1), getPixel(x + 1), getPixel(y + 1));
        }
        if (!left) {
            g.drawLine(getPixel(x), getPixel(y), getPixel(x), getPixel(y + 1));
        }
        if (!right) {
            g.drawLine(getPixel(x + 1), getPixel(y), getPixel(x + 1), getPixel(y + 1));
        }
        
    }
    
    /**
        * @brief paints component with clear background and borders
        * @param[in] "Graphics g" provided by JVM
    */
    public void paint(Graphics g) {
        clear(g);
    }
    
    /**
        * @brief clears Grid and draws new borders
        * @param[in] "Graphics g0" passed from paint() or repaint() by JVM
        * 
        * Also called by changeDimensions via repaint()
    */
    public void clear(Graphics g0) {
        Graphics2D g;
        
        g = (Graphics2D)g0;
        
        /* clear area */
        g.clearRect(0, 0, super.getSize().width, super.getSize().height);
        
        /* draw border */
        g.setStroke(lineStroke);
        g.drawLine(getPixel(0), getPixel(0), getPixel(cols), getPixel(0));
        g.drawLine(getPixel(0), getPixel(rows), getPixel(cols), getPixel(rows));
        g.drawLine(getPixel(0), getPixel(0), getPixel(0), getPixel(rows));
        g.drawLine(getPixel(cols), getPixel(0), getPixel(cols), getPixel(rows));
    }
    
    /**
        * @brief set size of maze -- dimensions must be recalculated
        * @param[in] "int width" width in pixels of Grid
        * @param[in] "int height" height in pixels of Grid
    */
    public void setSize(int width, int height) {
        super.setSize(width, height);
        changeDimensions(rows, cols);
    }
    
    /**
        * @brief set size of maze -- dimensions must be recalculated
        * @param[in] "Dimension d" dimensions in pixels of Grid
    */
    public void setSize(Dimension d) {
        this.setSize((int)d.getWidth(), (int)d.getHeight());
    }
    
    /**
        * @return # of rows
    */
    public int getNumRows() {
        return rows;
    }
    
    /**
        * @return # of columns
    */
    public int getNumCols() {
        return cols;
    }
    
}   // end of class
