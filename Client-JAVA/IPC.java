/**
    * @file "IPC.java"
    * @brief implements Inter-Process Communication (IPC) protocol
*/

import java.io.*;
import java.net.*;

/**
    * @brief implements IPC protocol
    * 
    * Opens a connection to a socket and sends data packets via a protocol to
    * the host.  Also receives and error checks messages from the host.
*/
public class IPC {
    
    /* constants */
    private final static char separator = ',';      // separates commands
    
    /* trackers */
    private Socket connection;                      // connection to host
    private InputStream in;                         // input stream from host (via socket)
    private OutputStream out;                       // output stream to host (via socket)
    
    /**
        * @brief opens connection to host via socket constructs IPC handle
        * @param[in] "int port" localhost port where host has server binded to
    */
    public IPC(int port) throws IOException {
        connection = new Socket("localhost", port);
        in = connection.getInputStream();
        out = connection.getOutputStream();
    }
    
    /**
        * @brief request host for new board
        * @param[in] "int numRows" # of rows
        * @param[in] "int numCols" @ of columns
    */
    public void newBoard(int numRows, int numCols) {
        try {
            sendLine(""
                    + (char)(COMMAND.toInt(COMMAND.NEW) + '0')
                    + ' '
                    + numRows + " "
                    + numCols
            );
        } catch (IOException e) {}
    }
    
    /**
        * @brief ask host to refresh maze
    */
    public void refreshBoard() {
        try {
            sendLine("" + (char)(COMMAND.toInt(COMMAND.REFRESH) + '0'));
        } catch (IOException e) {}
    }
    
    /**
        * @brief ask host to move
        * @param[in] "Direction direction" direction to move in
    */
    public void move(Direction direction) {
        try {
            sendLine(""
                    + (char)(COMMAND.toInt(COMMAND.MOVE) + '0')
                    + ' '
                    + (char)(Direction.toInt(direction) + '0')
            );
        } catch (IOException e) {}
    }
    
    /**
        * @brief ask host to toggle trace option
    */
    public void toggleTrace() {
        try {
            sendLine("" + (char)(COMMAND.toInt(COMMAND.TOGGLE_TRACE)+ '0'));
        } catch (IOException e) {}
    }
    
    /**
        * @brief close IPC
        * 
        * Ask host to close itself, and relinquish IPC resources such as sockets.
    */
    public void close() {
        try {
            sendLine(""+(char)(COMMAND.toInt(COMMAND.EXIT) + '0'));
            in.close();
            out.close();
            connection.close();
        } catch (IOException e) {}
    }
    
    /**
        * @brief read a command from the host
        * @return Command structure from host
    */
    HostMessage getCommand() throws IOException {
        HostMessage value;
        String line;
        String args[];      // arguments
        int num;
        int requiredArgs;
        
        /*
        STEPS
            1) get all arguments of line
            2) process first argument
                if invalid, treat as NOTHING command
            3) determine # of additional arguments
                if there are not enough additional arguments, treat as NOTHING command
            4) parse arguments to numbers
                if an argument is not a number, treat as NOTHING
            5) return
        */
        
        /* STEP 1 -- get all arguments on one line */
        line = readLine(80);
        args = line.split(" ");
        value = new HostMessage();
        /* END STEP 1 */
        
        /* STEP 2 -- process first argument */
        try {
            num = Integer.parseInt(args[0]);
            
            if (num < 0
            || (num > 3 && num < 8)
             || num > 9) {                      // invalid number?
                value.command = COMMAND.NOTHING;
            } else {
                value.command = COMMAND.valueOf(num);
            }
            
        } catch (NullPointerException e) {
            value.command = COMMAND.NOTHING;
        } catch (NumberFormatException e) {
            value.command = COMMAND.NOTHING;
        }
        /* END STEP 2 */
        
        /* STEP 3 -- determine # of additional arguments */
        switch (value.command) {
            case OVER:
                requiredArgs = 0;
                break;
                
            case CHANGE:
                // ARGS: x y CellStatus encodedConnectionBooleans
                requiredArgs = 4;
                break;
                
            case SEND_MESSAGE:
                requiredArgs = 0;       // DON'T CARE VALUE
                break;
                
            case DIM:
                requiredArgs = 2;
                break;
                
            case EXIT:
                requiredArgs = 0;
                break;
                
            case NOTHING:
            default:
                requiredArgs = 0;
                
        }
        /* END STEP 3 */
        
        /* STEP 4 -- parse arguments */
        if (value.command == COMMAND.SEND_MESSAGE) {            // special case?
            value.arg5 = line.substring(args[0].length() + 1, line.length());
        } else {
            
            if (args.length < requiredArgs) {
                value.command = COMMAND.NOTHING;
            } else {
                
                /* PARSE NUMBERS */
                try {
                    
                    if (requiredArgs > 0) value.arg1 = Integer.parseInt(args[1]);
                    if (requiredArgs > 1) value.arg2 = Integer.parseInt(args[2]);
                    if (requiredArgs > 2) value.arg3 = Integer.parseInt(args[3]);
                    if (requiredArgs > 3) value.arg4 = Integer.parseInt(args[4]);
                    
                } catch (NumberFormatException e) {
                    value.command = COMMAND.NOTHING;
                }
            } 
            
        }
        /* END STEP 4 */
        
        /* STEP 5 -- return */
        return value;
    }
    
    /**
        * @brief write a data packet to host
        * 
        * This private function is used by every sending function.
    */
    private void sendLine(final String str) throws IOException {
        String send;
        send = str + "" + ',';
        out.write(
                send.getBytes(java.nio.charset.Charset.forName("UTF-8"))
        );
    }
    
    /**
        * @brief read a data packet from host
        * 
        * This private function is used from getCommand function.
    */
    private String readLine(int max) throws IOException {
        
        int res;            // from read function
        char c;             // integer to character
        
        int length;         // length of string
        String str;         // return string
        
        str = "";
        length = 0;
        while (true) {
            
            res = in.read();
            c = (char)(res & 0xFF);
            
            if (res != -1) {                        // is valid character
                
                if (c == separator) {
                    return str;
                } else {
                    str += ""+c;
                    length++;
                    
                    if (length == max) {            // have you read max characters?
                        
                        do {
                            res = in.read();
                            c = (char)(res & 0xFF);
                        } while (res == -1 || c != separator);
                        
                        return str;
                    }
                }
            }   // end of "is valid character?"
        }       // end of infinite while loop
    }
    
}   // end of class
