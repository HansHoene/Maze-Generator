/**
    * @file "Direction.java"
    * @brief contains Direction enumeration
*/

/**
    * @brief enumeration for valid moves as directions
*/
public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT;
    
    /**
        * @brief resolves direction to integer
        * @param[in] "Direction val" enumeration value
        * @return direction as integer
    */
    public static int toInt(Direction val) {
        switch(val) {
            case UP:
                return 0;
                
            case DOWN:
                return 1;
                
            case LEFT:
                return 2;
                
            default:
                return 3;//right
        }
    }
    
    /**
        * @brief resolves integer to direction
        * @param[in] "int val" integer value
        * @return integer as direction
    */
    public static Direction valueOf(int val) {
        switch(val) {
            case 0:
                return UP;
                
            case 1:
                return DOWN;
                
            case 2:
                return LEFT;
                
            default:
                return RIGHT;//3
        }
    }
}