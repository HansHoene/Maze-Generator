/**
    * @file "CellStatus.java"
    * @brief Contains an enumeration for the types of cells, which determine colours
*/

/**
    * @brief enumeration for status of Cells
*/
public enum CellStatus {
    
    EMPTY,          // typical Cell
    TRACE,          // Cell has been traced along path
    SELECTED,       // Cell is current location; it is selected
    END;            // Cell is the destination
    
    /**
        * @brief resolves enumeration value to integer
        * @param "CellStatus status" the enumeration value
        * @return integer value of CellStatus
    */
    public static int toInt(CellStatus status) {
        switch(status) {
            case EMPTY:
                return 0;
                
            case TRACE:
                return 1;
                
            case SELECTED:
                return 2;
                
            default:
                    return 3;       // end
        }
    }
    
    /**
        * @brief resolves integer to enumeration value
        * @param "int val" integer value of enumeration
        * @return enumeration value corresponding to integer value
    */
    public static CellStatus valueOf(int val) {
        switch (val) {
            case 0:
                return EMPTY;
            case 1:
                return TRACE;
                
            case 2:
                return SELECTED;
                
            default:
                return END; // 3
        }
    }
    
} // end of class
