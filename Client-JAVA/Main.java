/**
    * @file "Main.java"
    * @brief JAVA Client entry point
*/

/**
    * @brief class with main method
*/
public class Main {
    
    /**
        * @brief main method -- start client and play game
        * @param[in] "String args[0]" port # that host server is binded to as a string
    */
    public static void main(String args[]) throws java.io.FileNotFoundException, java.io.IOException {
        
        Client.playGame(Integer.parseInt(args[0]));
        
    }

}
