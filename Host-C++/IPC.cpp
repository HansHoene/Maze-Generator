/**
    * @file "IPC.cpp"
*/

#include "IPC.hpp"

#include <string.h>
#include <fcntl.h>

/* IMPORTANT: rows are always sent before cols (y before x) */

IPC::IPC(unsigned int port) {
    connection = new Connection(port);
}

IPC::~IPC() {
    delete connection;
}

void IPC::sendMessage(const char str[]) {
    char message[1024];
    sprintf(message, "%c %s", SEND_MESSAGE + '0', str);
    sendLine(message);
}

void IPC::endGame() {
    char str[3];
    sprintf(str, "%c ", OVER + '0');
    sendLine(str);
}

void IPC::changeCell(unsigned int x, unsigned int y, CellStatus status, const Cell &data) {
    char str[48];
    int value;
    unsigned int i;

    value = 0;
    for (i = 0; i < 4; i++) {                   // for all directions
        value <<= 1;                            // shift bits left
        if (data.isConnected((Direction)i)) {   // is connected?
            value |= 0x1;                       // raise least-significant bit
        }
    }

    sprintf(str, "%c %d %d %d %d", CHANGE + '0', y, x, (int)status, value);
    sendLine(str);
}

void IPC::exit() {
    char str[3];
    sprintf(str, "%c ", EXIT + '0');
    sendLine(str);
}
void IPC::setNewDimension(unsigned int rows, unsigned int cols) {
    char str[100];
    sprintf(str, "%c %d %d", DIM + '0', rows, cols);
    sendLine(str);
}

void IPC::sendLine(const char buffer[]) {
    char separator;
    separator = SEPARATOR;
    connection->write(buffer, strlen(buffer));
    connection->write(&separator, 1);
}

Command IPC::getCommand() {

    Command command;

    char line[1024];    // line command string

    int temp;
    int flag;

    int requiredArgs;

    /*
    STEPS
        1) read line into character array and read line into integers via scanf
        2) determine if command is valid integer
            if not, convert command to (COMMAND)NOTHING
            otherwise, cast integer to correct enum COMMAND value
        3) determine how many additional arguments are required
        4) check if there are enough arguments
            if not, convert command to (COMMAND)NOTHING
        5) return command
    */

    /* STEP 1 -- read line into character array */
    readLine(line, 1024);
    flag = sscanf(line, "%d%d%d", &temp, &command.arg1, &command.arg2);

    /* STEP 2 -- determine if command is valid integer */
    if (flag == 0                                   // no command read?
        || temp < 4 || temp > 9) {                  // invalid command

        command.command = NOTHING;

    } else {
        command.command = (COMMAND)temp;
    }

    /* STEP 3 -- determine # of required additional arguments */
    switch (command.command) {
        case NEW:
            requiredArgs = 2;   // requires board dimensions
            break;
        case REFRESH:
            requiredArgs = 0;
            break;
        case MOVE:
            requiredArgs = 1;   // requires direction as integer
            break;
        case TOGGLE_TRACE:
            requiredArgs = 0;
            break;
        case EXIT:
            requiredArgs = 0;
            break;
        default:
            command.command = NOTHING;
            requiredArgs = 0;

    }

    /* STEP 4 -- check if there are enough arguments */
    if (flag - 1 < requiredArgs) {
        command.command = NOTHING;
    }

    return command;
}

void IPC::readLine(char buffer[], unsigned int max_size) {
    unsigned int length;    // length of stringso far
    unsigned int res;       // # of bytes read
    bool flag;              // should we exit?

    length = 0;
    res = 0;
    flag = false;
    while (1) {                                     // infinite loop

        if (res == 1) {                             // valid value read?

            if (buffer[length] == SEPARATOR) {      // was separator read?
                flag = true;
            } else {
                ++length;

                if (length == max_size - 1) {       // is buffer full?
                    flag = true;
                }

            }
        }

        if (flag) {                                 // should I exit? Or read another value?
            buffer[length] = '\0';
            return;
        } else {
            res = connection->read(&buffer[length], 1);
        }
    }
    return; // unreachable code
}
