/**
    * @file main.cpp
    * @brief main function -- sets everything up, reads commands, and sends commands
*/

#include "host.hpp"
#include "Maze_Builder.hpp"

#define CLIENT_NAME "client.jar"

#define PORT_NUMBER 666

int main() {

    
    Command client_request;
    Host *host;

    /* create unique filenames for IPC files */
    // tmpnam(inputFile);
    // tmpnam(outputFile);
    // I am using macros instead, now

    /* start JAVA client */
    #ifndef NO_JAVA_CLIENT
    {
        {
            char buffer[1024];
            sprintf(buffer, "START javaw -jar %s %d", CLIENT_NAME, PORT_NUMBER);
            system(buffer);
        }
    }
    #endif

    /* set up Host with IPC */
    host = new Host(PORT_NUMBER);

    /* IPC infinite loop */
    while (1) {

        client_request = host->getClientCommand();

        switch (client_request.command) {

            case NEW:

                if (client_request.arg1 < 2 || client_request.arg2 < 2) {                   // too small error?
                    host->sendMessage("Error: cannot have less than 2 rows or 2 columns");
                } else if (client_request.arg1 > ALDOUSBRODER_LIMIT 
                    || client_request.arg2 > ALDOUSBRODER_LIMIT) {                          // too big error?
                    host->sendMessage("Error: too many rows or columns");
                } else {
                    host->newMaze((unsigned int)client_request.arg1, (unsigned int)client_request.arg2);
                }
                break;

            case REFRESH:

                host->sendEntireMaze();
                break;

            case MOVE:

                if (client_request.arg1 < 0 || client_request.arg1 > 3) {                       // error?
                    host->sendMessage("Error: client program sent invalid move direction");
                } else {
                    host->makeMove((Direction)client_request.arg1);
                }
                break;

            case TOGGLE_TRACE:

                host->toggleTraceEnabled();
                break;

            case EXIT:

                delete host;
                return 0;
                break;

            case NOTHING:
            default:

                // NOTHING !!!
                break;

        }       // end of switch-statement
    }           // end of infinite while-loop
    return 0;   // unreachable return statement
}               // end of main function