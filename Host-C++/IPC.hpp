/**
    * @file IPC.hpp
    * @brief implements Inter-Process Communication (IPC) protocol
    *
    * This class implements the IPC protocol by parsing commands and
    * interpreting commands.  This object is a medium between the Socket-level
    * Communication layer and the Host.
*/

#ifndef IPC_HPP_
#define IPC_HPP_

#include "Maze.hpp"
#include "Connection.hpp"

#define SEPARATOR ','

/**
    * @brief valid protocol commands
*/
enum COMMAND {

    // from host to client
    OVER = 0,
    CHANGE = 1,
    SEND_MESSAGE = 2,
    DIM = 3,

    // from client to host
    NEW = 4,
    REFRESH = 5,
    MOVE = 6,
    TOGGLE_TRACE = 7,

    // can be sent be either one
    EXIT = 8,
    NOTHING = 9

};

/**
    * @brief valid status values of cells in grid
    *
    * The status of a Cell determines its coulour in the client GUI
*/
enum CellStatus {
    EMPTY = 0,          // typical Cell
    TRACE = 1,          // Cell has been traced along path
    SELECTED = 2,       // Cell is current location; it is selected
    END = 3             // Cell is the destination
};

/**
    * @brief holds a command with arguments sent from the client
*/
typedef struct {
    COMMAND command;
    int arg1;
    int arg2;
} Command;

/**
    * @brief implements Inter-Process Communication (IPC) protocol
*/
class IPC {

public:

    /**
        * @brief construct Inter-Process Communication (IPC) object
        * @param[in] "unsigned int port" port to connect Socket to
    */
    IPC(unsigned int port);

    /**
        * @brief destructs IPC object
    */
    ~IPC();

    /**
        * @brief send popup message to Client
        * @param[in] "const char str[]" message string (max = 1021 chars)
    */
    void sendMessage(const char str[]);

    /**
        * @brief tell Client that game is over
    */
    void endGame();

    /**
        * @brief tell Client that the status of a Cell has changed
        * @param[in] "unsigned int x" column of Cell
        * @param[in] "unsigned int y" row of Cell
        * @param[in] "CellStatus status" indicates status of (EMPTY, SELECTED, END, ...)
        * @param[in] "const Cell &data" the Cell that has changed
        *
        * The status of a Cell typically changes when a Cell is selected/unselected
        * or if a new board is generated.
    */
    void changeCell(unsigned int x, unsigned int y, CellStatus status, const Cell &data);

    /**
        * @brief tell Client to abruptly exit
        *
        * Use for unhandled errors
    */
    void exit();

    /**
        * @brief sets new dimensions
        * @param[in] "unsigned int rows" # of rows
        * @param[in] "unsigned int cols" # of cols
    */
    void setNewDimension(unsigned int rows, unsigned int cols);

    /**
        * @brief read Command from Client
        * @return Command from Client (NOTHING if no new commands)
    */
    Command getCommand();

private:

    /**
        * @brief sends line via socket
        * @param[in] "const char buffer[]" raw command data
        *
        * Used inside of public functions.  Replaces NULL terminator of string
        * with communication separator.
    */
    void sendLine(const char buffer[]);

    /**
        * @brief reads a line of input from the IPC socket layer
        * @param[out] "char buffer[]" unparsed command
        * @param[in] "unsigned int max" maximum # of chars that can be read
    */
    void readLine(char buffer[], unsigned int max_size);

    Connection *connection;      // where do I send the data?

};

#endif
