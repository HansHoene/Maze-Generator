/**
    * @file host.hpp
    * @brief manages Maze by sending and receiving data via an Inter-Process Communication (IPC) object.
    * @see IPC.hpp
    *
    * This object is created with two arguments: an input filename and an output
    * filename.  A file descriptor handle is created for each file.  The input
    * handle is read from for Inter-Process Communication (IPC).  The Client
    * process will be writing to that file.  The output handle will be written
    * to.  The Client process will be reading from that file.  The IPC object is
    * used to implement the protocol.  The Host class acts as a higher layer,
    * which can directly send mazes and other data.  The Host class can also
    * retrieve commands for the main function.  Upon destruction, the Host class
    * will relinquish file descriptor handles.
*/

#ifndef HOST_HPP_
#define HOST_HPP_

#include "Maze.hpp"
#include "Stack.hpp"
#include "IPC.hpp"

#define DEFAULT_DIMENSION 10

/**
    * @brief used to store coordinates
*/
class Coordinates {
public:
    unsigned int x;
    unsigned int y;
};

/**
    * @brief breaks complex operations into low-level protocol commands
*/
class Host {

public:

    /**
        * @brief construct a Host object for communicating with Client
        * @param[in] "char inputFile[]" input file path
        * @param[in] "char outputFile[]" output file path
    */
    Host(unsigned int port);

    /**
        * @brief close resources
    */
    ~Host();

    /**
        * @brief generate a new maze and sends it to client
        * @param[in] "unsigned int numRows" # of rows
        * @param[in] "unsigned int numCols" # of cols
    */
    void newMaze(unsigned int numRows, unsigned int numCols);

    /**
        * @brief determines if trace is enabled
        * @return true if trace is enabled
    */
    bool getTraceEnabled() const;

    /**
        * @brief toggles boolean trace setting
    */
    void toggleTraceEnabled();

    /**
        * @brief tries to make a move and sends it to client
        * @param[in] "Direction direction" direction of the move
    */
    void makeMove(Direction direction);

    /**
        * @brief reads command from client
        * @return Command from client
    */
    Command getClientCommand();

    /**
        * @brief sends message to client
        * @param[in] "const char str[]" message to client
    */
    void sendMessage(const char str[]);

    /**
        * @brief sends all Maze data to client
        *
        * Used for sending new mazes (after updating dimensions) and for
        * refreshing a maze.
    */
    void sendEntireMaze();

private:

    Maze *maze;                     // Client maze
    IPC *ipc;                       // Implements Inter-Process Communication (IPC) Protocol
    Stack<Coordinates> *path;       // traces path that Client has traced with moves
    bool traceEnabled;              // true if Cells along trace path receive a special colour
    unsigned int rows,
                 cols;              // maze dimensions

};

#endif
