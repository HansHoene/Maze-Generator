/**
    * @file "host.cpp"
*/

#include "host.hpp"
#include "Maze_Builder.hpp"

#include <fcntl.h>

Host::Host(unsigned int port) {
    /*
    STEPS
        1) open handles to IPC via socket port
        2) set trace boolean
            also, set maze and other values to dont-care values.
            In step 4, creating a maze with default arguments
            deletes current values, so they cannot be NULL.
        3) create maze with default arguments
    */

    /* STEP 1 -- open handles to files and set up IPC */
    ipc = new IPC(port);
    /* END STEP 1 */

    /* STEP 2 -- set up boolean trace and random values */
    traceEnabled = true;
    rows = 2;                               // don't care
    cols = 2;                               // don't care
    maze = new Maze(2, 2, true);            // don't care
    path = new Stack<Coordinates>(1);            // don't care
    /* END STEP 2 */

    /* STEP 3 -- create maze with default arguments */
    newMaze(DEFAULT_DIMENSION, DEFAULT_DIMENSION);
    /* END STEP 3 */
}

Host::~Host() {
    delete maze;
    delete path;
    delete ipc;
}

void Host::newMaze(unsigned int numRows, unsigned int numCols) {
    Coordinates tmp;

    /*
    STEPS
        1) free maze and path
        2) create new maze
            change # rows
            change # cols
            new maze
            new path
        3) notify client
            tell client to change dimension
            send entire maze
    */

    /* ERROR CHECK */
    if (numRows < 2 || numCols < 2) {
        return;
    }
    if (numRows > ALDOUSBRODER_LIMIT || numCols > ALDOUSBRODER_LIMIT
        || numRows > RECURSIVEBACKTRACKER_LIMIT || numCols > RECURSIVEBACKTRACKER_LIMIT) {
        return;
    }

    /* STEP 1 -- free maze and path */
    delete maze;
    delete path;
    maze = NULL;                                    // maze must be NULL for function call in Step 2
    path = NULL;
    /* END STEP 1 */

    /* STEP 2 -- create new maze */
    rows = numRows;
    cols = numCols;
    createMaze(&maze, rows, cols);                  // generate maze
    path = new Stack<Coordinates>(rows * cols);     // size assumes that the path can contain all Cells in maze
    tmp.x = 0;
    tmp.y = 0;
    path->push(tmp);                                // add first Cell to path
    /* END STEP 2 */

    /* STEP 3 -- notify client */
    ipc->setNewDimension(rows, cols);
    sendEntireMaze();
    /* END STEP 3 */
}

bool Host::getTraceEnabled() const {
    return traceEnabled;
}

void Host::toggleTraceEnabled() {
    Stack<Coordinates> copy(*path);     // copy of path
    CellStatus target;

    copy.pop();                         // pop current location from path
    traceEnabled = !(traceEnabled);     // toggle trace boolean

    /* select value of all Cells in path */
    if (traceEnabled) {
        target = TRACE;
    } else {
        target = EMPTY;
    }

    /* update all Cells in path */
    while (!copy.isEmpty()) {

        ipc->changeCell(copy.top().x, copy.top().y, 
            target, *maze->getCell(copy.top().x, copy.top().y));    // update Cell in path
        copy.pop();                                                 // remove Cell om copy of path

    }

}

void Host::makeMove(Direction direction) {

    Coordinates current;            // current selected location
    Coordinates destination;        // location determined by direction

    current = path->top();          // determine currently selected Cell
    path->pop();                    // remove currently selected Cell from path
                                    // towards the end, you may need to add this back

    /* ERROR CHECK */
    if (!maze->getCell(current.x, current.y)
        ->isConnected(direction)) {                     // invalid move?

        path->push(current);                // add current back to path
        return;                             // return without sending any changes
    }

    /* determine destination coordinates based on direction */
    destination = current;
    switch (direction) {
        case UP:
            --destination.y;
            break;
        case DOWN:
            ++destination.y;
            break;
        case LEFT:
            --destination.x;
            break;
        default:
            ++destination.x;
    }

    /* actually make the move and send updates */
    if (path->top().x == destination.x 
        && path->top().y == destination.y) {                            // going backwards over path?
        // in this case, current is no longer part of the path because
        // the user went backwards over a path that was already taken.
        // Therefore, leave the path as it is and send appropriate updates.

        ipc->changeCell(current.x, current.y, 
            EMPTY, *maze->getCell(current.x, current.y));               // current is now empty
        ipc->changeCell(destination.x, destination.y, 
            SELECTED, *maze->getCell(destination.x, destination.y));    // destination is now selected

    } else {                                                            // going forwards?
        // Since you are going to a new Cell, add current back to
        // path and add new Cell to top of path as selected Cell.
        // Then send updates.

        path->push(current);
        path->push(destination);

        if (traceEnabled) {                                             // is trace enabled?
            ipc->changeCell(current.x, current.y,
                TRACE, *maze->getCell(current.x, current.y));           // current is now part of path
        } else {
            ipc->changeCell(current.x, current.y,
                EMPTY, *maze->getCell(current.x, current.y));           // current is part of path but it is displayed as EMPTY
        }

        ipc->changeCell(destination.x, destination.y,
            SELECTED, *maze->getCell(destination.x, destination.y));    // destination is now selected
    }

    if (destination.x == cols - 1
        && destination.y == rows - 1) {                                 // is game over?
        ipc->endGame();
    }

}

void Host::sendEntireMaze() {
    unsigned int i, j;

    CellStatus *arr;                // array of Cell statuses
    Stack<Coordinates> copy(*path); // copy of Stack
    Coordinates location;

    /*
    STEPS
        1) create array of CellStatus
            initialise everything with EMPTY
            set last Cell as end
        2) loop through all Cells sending data
    */

    /* STEP 1 -- create array of Cell status */
    arr = new CellStatus[rows * cols];              // create array

    // set all values to EMPTY to start
    for (i = 0; i < rows; i++) {
        for (j = 0; j < cols; j++) {
            arr[(i * cols) + j] = EMPTY;
        }
    }

    // set selected Cell
    location = copy.top();
    arr[(location.y * cols) + location.x] = SELECTED;
    copy.pop();

    // trace path if trace is enabled
    if (traceEnabled) {

        while (!copy.isEmpty()) {
            location = copy.top();
            arr[(location.y * cols) + location.x] = TRACE;
            copy.pop();
        }

    }
    
    arr[(rows * cols) - 1] = END;                   // set last Cell as end
    /* END STEP 1 */

    /* STEP 2 -- send data to client */
    for (i = 0; i < rows; i++) {
        for (j = 0; j < cols; j++) {
            ipc->changeCell(j, i, arr[(i * cols) + j], *maze->getCell(j, i));
        }
    }
    /* END STEP 2 */

    /* free data */
    delete[] arr;
}

Command Host::getClientCommand() {
    return ipc->getCommand();
}

void Host::sendMessage(const char str[]) {
    ipc->sendMessage(str);
}