/**
    * @file Connection.hpp
    * @brief sends and reads over a socket
    *
    * This class is platform specific.
*/

#ifndef CONNECTION_HPP_
#define CONNECTION_HPP_

/*
For the two OS covered here, several communication macros are defined for
platform-independant source code.
*/

#ifdef WINDOWS

#include <WinSock2.h>
#include <Windows.h>

typedef SOCKET SocketHandle;

#define STARTUP() { WSADATA wsa; WSAStartup(MAKEWORD(2, 2), &wsa); }
#define WRITE(HANDLE, BUFFER, LENGTH) send(HANDLE, BUFFER, LENGTH, 0)
#define READ(HANDLE, BUFFER, LENGTH) recv(HANDLE, BUFFER, LENGTH, 0)
#define CLOSE(HANDLE) closesocket(HANDLE)
#define CLEANUP() WSACleanup()

#else
#ifdef LINUX

#include <sys/socket.h>
#include <netdb.h>          // gethostbyname, struct hostent

#include <stdio.h>          // fprintf
#include <unistd.h>         // close (a file descriptor)

typedef int SocketHandle;

#define STARTUP() {}
#define WRITE(HANDLE, BUFFER, LENGTH) write(HANDLE, BUFFER, LENGTH)
#define READ(HANDLE, BUFFER, LENGTH) read(HANDLE, BUFFER, LENGTH)
#define CLOSE(HANDLE) close(HANDLE)
#define CLEANUP() {}

#else
#error "Must define OS flags!"
#endif
#endif

/**
    * @brief lowest level of Inter-Process Communication (IPC)
    *
    * This class uses sockets to send raw data to client.  Function macros for
    * platform-specific functions are decided by above.
*/
class Connection {

public:

    /**
        * @brief opens a Socket and uses it for low-level Inter-Process Communication (IPC)
        * @param[in] "unsigned int port" port number of Socket
    */
    Connection(unsigned int port);

    /**
        * @brief destructs object and closes Sockets
    */
    ~Connection();

    /**
        * @brief writes to socket
        * @param[in] "const char buffer[]" characters to write to socket
        * @param[in] "unsigned it length" # of bytes to write to socket
        * @return # of bytes that were actually sent
        * @see IPC.hpp
    */
    int write(const char buffer[], unsigned int length);

    /**
        * @brief reads from socket
        * @param[out] "char buffer[]" characters read from socket
        * @param[in] "unsigned int length" # of characters to read
        * @return the # of bytes that were read

    */
    int read(char buffer[], unsigned int length);

private:

    SocketHandle sock;              // socket connected to Client

};

#endif
