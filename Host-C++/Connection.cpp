/**
    * @file "Connection.cpp"
*/

#include "Connection.hpp"

Connection::Connection(unsigned int port) {

    SocketHandle server;

    struct sockaddr_in server_addr = { 0 };
    struct sockaddr_in client_addr = { 0 };

    int client_addr_size;

    STARTUP();
    client_addr_size = sizeof(struct sockaddr_in);

    /* initialise server socket */
    server = socket(AF_INET, SOCK_STREAM, PF_UNSPEC);
    if (server == INVALID_SOCKET) {
        throw "Could not open server socket";
    }

    /* determine localhost address */
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(port);             // localhost address

                                                    /* bind server to localhost address */
    if (
        bind(server, (struct sockaddr *)&server_addr, sizeof(server_addr))
        == SOCKET_ERROR) {
        throw "Could not bind server to localhost address";
    }

    /* listen for incoming connections */
    listen(server, 0);

    /* accept a connection */
    do {
        sock = accept(server, (struct sockaddr *)&client_addr,
            (int *)&client_addr_size);
    } while (sock == INVALID_SOCKET);

    /* close server */
    CLOSE(server);
}

Connection::~Connection() {
    CLOSE(sock);
    CLEANUP();
}

int Connection::write(const char buffer[], unsigned int length) {
    return WRITE(sock, buffer, length);
}

int Connection::read(char buffer[], unsigned int length) {
    return READ(sock, buffer, length);
}
