/**
    * @file Stack.hpp
    * @brief implements a templated Stack data structure
*/

#ifndef STACK_HPP_
#define STACK_HPP_

/**
    * @brief templated Stack class
*/
template <class Item>
class Stack {

public:

    Stack(const Stack<Item> &stack);
    Stack(unsigned int maximumSize);
    ~Stack();

    bool isEmpty();
    bool isFull();

    void push(Item item);
    void pop();
    Item top();

private:

    unsigned int limit;         // maximum # of allowable items
    unsigned int size;          // current size of Stack

    Item *arr;                  // array of stored items
};

#include "Stack.hpp"

template <class Item>
Stack<Item>::Stack(const Stack<Item> &stack) {
    unsigned int i;

    limit = stack.limit;
    size = stack.size;
    arr = new Item[limit];

    for (i = 0; i < size; i++) {
        arr[i] = stack.arr[i];
    }
}

template <class Item>
Stack<Item>::Stack(unsigned int maximumSize) {
    limit = maximumSize;
    size = 0;
    arr = new Item[limit];
}

template <class Item>
Stack<Item>::~Stack() {
    delete[] arr;
}

template <class Item>
bool Stack<Item>::isEmpty() {
    return (size == 0);
}

template <class Item>
bool Stack<Item>::isFull() {
    return (size == limit);
}

template <class Item>
void Stack<Item>::push(Item item) {
    if (!isFull()) {
        arr[size] = item;
        ++size;
    }
}

template <class Item>
void Stack<Item>::pop() {
    if (!isEmpty()) {
        --size;
    }
}

template <class Item>
Item Stack<Item>::top() {
    if (!isEmpty()) {
        return arr[size - 1];
    } else {
        return arr[0];
    }
}

#endif
