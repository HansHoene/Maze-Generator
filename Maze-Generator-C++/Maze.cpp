#include "Maze.hpp"
#include <stdlib.h>     // malloc, free, NULL
#include <iostream>

/* CAUTION: index = row * NUMCOLS + cols (not NUMROWS); this caused me a MAJOR BUG */

Cell::Cell() {

    unsigned int i;

    for (i = 0; i < 4; i++) {
        connections[i] = false;
        neighbours[i] = NULL;
    }
}

void Cell::initialise(Cell *neighbours[], bool open, unsigned int row, unsigned int col) {

    /* CAREFUL: two variables named neighbours; use "this" keyword when applicable */

    unsigned int i;
    bool flag;          // is Cell already initialised?

    this->row = row;
    this->col = col;

    /* set */
    flag = false;
    for (i = 0; i < 4 && !flag; i++) {
        if (this->neighbours[i] != NULL) {
            flag = true;
        }
    }

    /* only initialise if never initialised before */
    if (!flag) {                            // is Cell uninitialised?
        for (i = 0; i < 4; i++) {
            this->neighbours[i] = neighbours[i];

            if (neighbours[i] == NULL) {    // are there no neighbours?
                                            // cannot be a connection
                this->connections[i] = false;
            } else {
                this->connections[i] = open;
            }

        }
    }

    
}

bool Cell::isConnected(Direction direction) const {
    return connections[direction];
}

void Cell::setConnected(Direction direction, bool connected) {

    Direction invert;

    if (neighbours[direction] != NULL) {        // does cell have neighbour in this direction?
        connections[direction] = connected;     // set boolean connected

        // update neighbours connections
        switch (direction) {
            case UP:
                invert = DOWN;
                break;
            case DOWN:
                invert = UP;
                break;
            case LEFT:
                invert = RIGHT;
                break;
            //case RIGHT:
            default:
                invert = LEFT;
                break;
        }
        neighbours[direction]->connections[invert] = connected;

    }
    // otherwise, no connection shall be made
}

Cell *Cell::getNeighbour(Direction direction) const {
    return neighbours[direction];
}

unsigned int Cell::getRow() const {
    return row;
}

unsigned int Cell::getCol() const {
    return col;
}

Maze::Maze(unsigned int width, unsigned int height, bool open) {

    unsigned int row, col;      // loop variables
    Cell *neighbours[4];        // stores neighbours to pass to Cell constructors

    this->numCols = width;
    this->numRows = height;
    this->cells = new Cell[width * height];

    /* initialise every cell */
    for (row = 0; row < numRows; row++) {
        for (col = 0; col < numCols; col++) {

            /*
            For every cell:
                1) get neighbours
                2) initialise Cell with pointers to neighbours
            */

            // get neighbour from above
            if (row > 0) {
                neighbours[UP] = &cells[
                    ((row - 1) * numCols) + col     // go up one row
                ];
            } else {
                neighbours[UP] = NULL;
            }

            // get neighbour from below
            if (row < numRows - 1) {
                neighbours[DOWN] = &cells[
                    ((row + 1) * numCols) + col    // go down one row
                ];
            } else {
                neighbours[DOWN] = NULL;
            }

            // get neighbour from left
            if (col > 0) {
                neighbours[LEFT] = &cells[
                    (row * numCols) + col - 1   // go down a column
                ];
            } else {
                neighbours[LEFT] = NULL;
            }

            // get neighbour from right
            if (col < numCols - 1) {
                neighbours[RIGHT] = &cells[
                    (row * numCols) + col + 1   // go up a column
                ];
            } else {
                neighbours[RIGHT] = NULL;
            }

            cells[(row * numCols) + col].initialise(neighbours, open, row, col);  // initailise cell
        }
    }
}

Maze::~Maze() {
    /* call destructors for every cell */
    delete cells;
}

Cell *Maze::getCell(unsigned int x, unsigned int y) const {
    if (x < numCols         // valid horizontal offset?
     && y < numRows) {      // valid vertical offset?

        return &cells[(y * numCols) + x];
    } else {
        return NULL;
    }
}

unsigned int Maze::getNumRows() const {
    return numRows;
}

unsigned int Maze::getNumCols() const {
    return numCols;
}

std::ostream &operator<<(std::ostream &out, const Maze &maze) {
    unsigned int i, j;

    /*
    STEPS
        1) print top border
        2) print every row
            for each cell, print bottom, then right side
            every row shall start with left border
    */

    /* print top border */
    out.put(' ');
    for (i = 0; i < maze.numCols; i++) {
        // 2 underscores -- one for side border, one for lower borders
        out.put('_');
        out.put(' ');
    }
    out.put('\n');

    /* iterate through columns inside every row for proper printing order */
    for (i = 0; i < maze.numRows; i++) {
        out << '|';            // print start of left border
        for (j = 0; j < maze.numCols; j++) {
            
            if (maze.getCell(j, i)->isConnected(DOWN)) {        // connected down?
                out.put(' ');
            } else {
                out.put('_');
            }

            if (maze.getCell(j, i)->isConnected(RIGHT)) {       // connected right?
                out.put(' ');
            } else {
                out.put('|');
            }

        }
        out.put('\n');
    }

    return out;
}
