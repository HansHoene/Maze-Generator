/**
    * @file Maze.hpp
    * @brief contains Maze data structure and Cell data structure
    *
    * A Maze contains a large array of Cells.  Each Cell has neighbours, which
    * can be either connected or disconnected to one another.  All functions
    * safely work on the Maze.
*/

#ifndef MAZE_HPP_
#define MAZE_HPP_

#include <iostream>

enum Direction {
    UP = 0,
    DOWN = 1,
    LEFT = 2,
    RIGHT = 3
};

class Cell {

public:

    /**
        * @brief initialises empty Cell -- can only be filled once using initialise function
        * @see Cell::initialise(Cell *neighbours[], bool open);
    */
    Cell();

    /**
        * @brief determines if GridCell is connected to a neighbour
        * @param[in] "Direction direction" direction of neighbour
        * @return true if connected
    */
    bool isConnected(Direction direction) const;

    /**
        * @brief connects or disconnects a neighbour
        * @param[in] "Direction direction" direction of neighbour
        * @param[in] "bool connected" true to connect, false to disconnect
    */
    void setConnected(Direction direction, bool connected);

    /**
        * @param[in] "Direction direction" direction of neighbour
        * @return pointer to neighbouring GridCell
    */
    Cell *getNeighbour(Direction direction) const;

    /**
        * @brief initialise Cell
        * @param[in] "Cell *neighbours[]" array of pointers to 4 neighbours
        * @param[in] "bool open" should existing connections be left open? Otherwise, closed.
        * @param[in] "unsigned int row" row #
        * @param[in] "unsigned int col" column #
    */
    void initialise(Cell *neighbours[], bool open, unsigned int row, unsigned int col);

    unsigned int getRow() const;

    unsigned int getCol() const;

private:

    Cell *neighbours[4];        // pointers to neighbours in all directions
    bool connections[4];        // is connected to neighbour in a particular direction?

    unsigned int row,
                col;

};

class Maze {

    /*
    NOTE:
        row 0 = top
        last row = bottom
        col 0 = left
        last col = right
    */

public:

    /**
        * @brief initialises a Maze
        * @param[in] "unsigned int width" # of columns
        * @param[in] "unsigned int height" # of rows
        * @param[in] "bool open" should board be left open? Otherwise, filled in.
    */
    Maze(unsigned int width, unsigned int height, bool open);

    /**
        * @brief destroys Maze
    */
    ~Maze();

    /**
        * @brief get a GridCell within the Grid
        * @param[in] "unsigned int x" column index
        * @param[in] "unsigned int y" row index
        * @return pointer to a Cell at coordinates (x, y)
    */
    Cell *getCell(unsigned int x, unsigned int y) const;

    /**
        * @return number of rows
    */
    unsigned int getNumRows() const;

    /**
        * @return number of columns
    */
    unsigned int getNumCols() const;

    /**
        * @brief prints Maze
        * @param[in] "std::ostream &out" output stream
        * @param[in] "const Maze &maze" Maze
        * @return the same output stream (used for chaining)
    */
    friend std::ostream &operator<<(std::ostream &out, const Maze &maze);

private:

    Cell *cells;                        // array of Cells
    unsigned int numRows,               // # of rows
                 numCols;               // # of cols

};

#endif
