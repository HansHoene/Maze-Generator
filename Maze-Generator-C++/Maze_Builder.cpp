#include "Maze.hpp"
#include "Maze_Builder.hpp"
#include <stdlib.h>
#include <time.h>
#include "Stack.hpp"

bool createMaze_AldousBroder(Maze **maze, const unsigned int numRows, const unsigned int numCols) {
    /* http://weblog.jamisbuck.org/2011/1/17/maze-generation-aldous-broder-algorithm */

    unsigned int i, j;              // loop variables

    Cell *node;
    unsigned int numCells;          // # of remaining cells that have not been visited
    bool **visited;                 // 2D array of Cells that have been visited
    Direction random;               // random direction
    unsigned int row, col;

    /*
    STEPS
        1) set up random # generator
        2) input error check
        3) set up dynamic tracking variables (like Maze, numCells, visited, ...)
        4) run algorithm
        5) free dynamically allocated data
    */

    /* STEP 1 -- random number initialiser */
    srand((unsigned)time(NULL));
    /* END STEP 1 */

    /* STEP 2 -- input error check */
    if (*maze != NULL
        || numRows < 2 || numCols < 2 
        || numRows > ALDOUSBRODER_LIMIT || numCols > ALDOUSBRODER_LIMIT) {
        return false;
    }
    /* END STEP 2 */

    /* STEP 3 -- set up variables */
    *maze = new Maze(numCols, numRows, false);
    numCells = numRows * numCols;
    visited = new bool *[numRows];
    for (i = 0; i < numRows; i++) {
        visited[i] = new bool[numCols];

        for (j = 0; j < numCols; j++) {
            visited[i][j] = false;
        }
    }
    /* END STEP 3 */

    /* STEP 4 -- run algorithm */
    row = 0;// rand() % maze->getNumRows();
    col = 0;// rand() % maze->getNumCols();
    node = (*maze)->getCell(col, row);
    visited[row][col] = true;
    --numCells;

    while (numCells > 0) {                      // while there are cells that have not been traversed
        /*
        STEPS
            1) get random direction (if no neighbour there, then try again)
            2) move (row, col) variables in that direction
            3) if neighbour not visited yet,
                connect to neighbour
                toggle visited boolean
                decrement numCells
            4) set node to neighbour in that direction
        */

        /* get random direction */
        do {
            random = (Direction)(rand() % 4);
        } while (node->getNeighbour(random) == NULL);     // repeat until you get a neighbour that exists

        /* move (row, col) */
        switch (random) {
            case UP:
                row--;
                break;
            case DOWN:
                row++;
                break;
            case LEFT:
                col--;
                break;
            default:
                col++;
        }

        /* if not visited, handle */
        if (!visited[row][col]) {               // not visited?
            node->setConnected(random, true);
            visited[row][col] = true;
            --numCells;
        }

        node = node->getNeighbour(random);
    }
    /* END STEP 4 */

    /* STEP 5 -- free memory */
    for (i = 0; i < numRows; i++) {
        delete visited[i];
    }
    delete visited;
    /* END STEP 5 */

    return true;
}

bool createMaze_RecursiveBacktracker(Maze **maze, const unsigned int numRows, const unsigned int numCols) {

    /*
    Recursive Backtracker Algorithm
        1) create secluded maze
        2) create stack of Cells
        3) push first Cell into stack
        4) generate maze...
            while the stack is not empty
                if stack's top Cell has neighbours
                    choose a random neighbour
                    open passage to neighbour
                    push neighbour to stack
                else
                    pop from stack
                end-if
            end-while
    */

    Stack<Cell *> *stack;            // stack of Cells
    Cell *neighbours[4];             // variables to temporarily store neighbours
    unsigned int numNeighbours;
    bool *visited;                   // array containing which Cells have been visited

    unsigned int i;

    /* error check */
    if (numRows < 2 || numCols < 2
        || *maze != NULL
        || numRows > RECURSIVEBACKTRACKER_LIMIT || numCols > RECURSIVEBACKTRACKER_LIMIT) {
        return false;
    }

    /* set up algorithm variables */
    *maze = new Maze(numCols, numRows, false);          // generate maze of given dimensions
    stack = new Stack<Cell *>(numRows * numCols);       // create stack large enough to hold all Cells
    visited = new bool[numRows * numCols];              // set up array (remember to initialise everything to false)
    srand((unsigned)time(NULL));                        // set up random number generator

    for (i = 0; i < numRows * numCols; i++) {
        visited[i] = false;
    }
    stack->push((*maze)->getCell(rand() % numCols, rand() % numRows));                // push first Cell to stack
    visited[
        (stack->top()->getRow() * numCols) + stack->top()->getCol()
    ] = true;

    /* execute algorithm */
    while (!stack->isEmpty()) {

        /* determine if Cell has unvisited neighbours */
        numNeighbours = 0;
        for (i = 0; i < 4; i++) {                               // loop through all directions
            neighbours[numNeighbours] =
                stack->top()->getNeighbour((Direction)i);         // add neighbour to array
                                                                // but do not increment numNeighbours until you know that neighbour is unvisited

            if (neighbours[numNeighbours] != NULL) {            // neighbour is non-NULL?
                if (!visited[
                    (neighbours[numNeighbours]->getRow() * numCols)
                        + neighbours[numNeighbours]->getCol()
                ]) {                                            // neighbour is not visited?
                    ++numNeighbours;
                }
            }

        }

        if (numNeighbours == 0) {                               // no neighbours?
            stack->pop();
        } else {

            neighbours[0] = neighbours[rand() % numNeighbours]; // move a random neighbour to index 0

            /* connect to random neighbour (neighbours[0]) */
            for (i = 0; i < 4; i++) {
                if (stack->top()->getNeighbour((Direction)i) 
                    == neighbours[0]) {                         // is the random neighbur in direction i?
                    stack->top()->
                        setConnected((Direction)i, true);       // connect to neighbour in this direction
                }
            }

            stack->push(neighbours[0]);                         // push random unvisited neighbour to top of stack

            visited[
                (neighbours[0]->getRow() * numCols)
                    + neighbours[0]->getCol()
            ] = true;                                           // set neighbour as visited
        }
    }   // end while-loop

    /* free resources */
    delete[] visited;
    delete stack;

    return true;            // successful

}

bool createMaze(Maze **maze, const unsigned int numRows, const unsigned int numCols) {

    unsigned int random;

    srand((unsigned)time(NULL));
    random = rand() % 10;

    switch (random) {
        case 0:
        case 1:
        case 2:
        case 3:
            return createMaze_AldousBroder(maze, numRows, numCols);
        case 4:
        case 5:
        case 6:
        case 7:
            return createMaze_RecursiveDivsion(maze, numRows, numCols);
        case 8:
        case 9:
            return createMaze_RecursiveBacktracker(maze, numRows, numCols);
    }

    return false;       // unreachable code

}

bool createMaze_RecursiveDivsion(Maze **maze, const unsigned int numRows, const unsigned int numCols) {
    Stack<Area> *stack;                          // stack of areas to divide
    Area parent;
    Area leftUp, rightUp, leftDown, rightDown;

    Cell *cell;
    unsigned int i;
    unsigned int random;
    unsigned int random_exclude;

    /* error check */
    if (numRows < 2 || numCols < 2
        || *maze != NULL
        || numRows > RECURSIVEDIVISION_LIMIT || numCols > RECURSIVEDIVISION_LIMIT) {
        return false;
    }

    /* set up algorithm variables */
    *maze = new Maze(numCols, numRows, true);           // generate maze w/ no walls of given dimensions
    stack = new Stack<Area>(numRows * numCols);
    srand((unsigned)time(NULL));                        // set up random number generator

    parent.topLeft = (*maze)->getCell(0, 0);
    parent.width = (*maze)->getNumCols();
    parent.height = (*maze)->getNumRows();
    stack->push(parent);

    while (!stack->isEmpty()) {
        parent = stack->top();
        stack->pop();

        if (parent.height >= 2 && parent.width >= 2) {

            /* use leftUp and rightUp and left division and right division */
            /* divide parent into a left and right division */
            leftUp.height = parent.height;
            rightUp.height = parent.height;                     // children have same height as parent

            leftUp.width = (rand() % (parent.width - 1)) + 1;
            rightUp.width = parent.width - leftUp.width;        // give children random width

            leftUp.topLeft = parent.topLeft;
            rightUp.topLeft = (*maze)->getCell(
                parent.topLeft->getCol() + leftUp.width,
                parent.topLeft->getRow()
            );

            /* divide leftUp into leftUp and leftDown */
            leftDown.width = leftUp.width;
            leftDown.height = (rand() % (leftUp.height - 1)) + 1;
            leftUp.height = leftUp.height - leftDown.height;
            leftDown.topLeft = (*maze)->getCell(
                leftUp.topLeft->getCol(),
                leftUp.topLeft->getRow() + leftUp.height
            );

            /* divide rightUp into rightUp and rightDown */
            rightDown.width = rightUp.width;
            rightDown.height = leftDown.height;
            rightUp.height = leftUp.height;
            rightDown.topLeft = (*maze)->getCell(
                rightUp.topLeft->getCol(),
                rightUp.topLeft->getRow() + rightUp.height
            );

            /* fill in borders */
            random_exclude = rand() % 4;

            /* fill in border between top-left and top-right */
            if (random_exclude == 0) {
                random = leftUp.height;     // will leave no holes!
            } else {
                random = rand() % leftUp.height;
            }
            cell = rightUp.topLeft;
            for (i = 0; i < leftUp.height; i++) {
                if (i != random) {
                    cell->setConnected(LEFT, false);    // draw border except for one random value
                }
                cell = cell->getNeighbour(DOWN);
            }

            /* fill in border between top-right and bottom-right */
            if (random_exclude == 1) {
                random = rightUp.width;     // will leave no holes!
            } else {
                random = rand() % rightUp.width;
            }
            cell = rightDown.topLeft;
            for (i = 0; i < rightUp.width; i++) {
                if (i != random) {
                    cell->setConnected(UP, false);    // draw border except for one random value
                }
                cell = cell->getNeighbour(RIGHT);
            }

            /* fill in border between bottom-right and bottom-left */
            if (random_exclude == 2) {
                random = leftDown.height;     // will leave no holes!
            } else {
                random = rand() % leftDown.height;
            }
            cell = rightDown.topLeft;
            for (i = 0; i < leftDown.height; i++) {
                if (i != random) {
                    cell->setConnected(LEFT, false);    // draw border except for one random value
                }
                cell = cell->getNeighbour(DOWN);
            }

            /* fill in border between bottom-left and top-left */
            if (random_exclude == 3) {
                random = leftUp.width;     // will leave no holes!
            } else {
                random = rand() % leftUp.width;
            }
            cell = leftDown.topLeft;
            for (i = 0; i < leftUp.width; i++) {
                if (i != random) {
                    cell->setConnected(UP, false);    // draw border except for one random value
                }
                cell = cell->getNeighbour(RIGHT);
            }

            /* add divisions to stack for further dividing */
            stack->push(leftUp);
            stack->push(rightUp);
            stack->push(leftDown);
            stack->push(rightDown);

        }

    }

    delete stack;
    return true;
}
