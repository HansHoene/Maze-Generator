#include "Maze.hpp"
#include "Maze_Builder.hpp"
#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main() {
    unsigned int i;
    unsigned int row, col;          // user input
                                    // # of rows and # of cols
                                    // reused as coordinates in loop

    Maze *maze;                     // maze that shall be generated
    /*
    STEPS
        1) get # rows input
        2) get # cols input
        3) print output
        4) free dynamically allocated data
    */

    /* read # of rows */
    do {

        cout << "Enter # of rows in maze: ";                    // prompt for user input
        if (!(cin >> row)) {                                    // is input NaN?
            cout << "Error: input is not an unsigned integer"
                << endl;
            return 0;
        }

        if (row > ALDOUSBRODER_LIMIT || row < 2) {                           // is bad input?
            cout << "Error: input must at least 2 but below "
                << ALDOUSBRODER_LIMIT << endl;
            i = 1;                                              // set flag
        } else {
            i = 0;
        }

    } while (i);                                                // continue prompting for user input while flag is set

    /* read # of columns */
    do {

        cout << "Enter # of columns in maze: ";                  // prompt for user input
        if (!(cin >> col)) {                                     // is input NaN?
            cout << "Error: input is not an unsigned integer"
                << endl;
            return 0;
        }

        if (col > ALDOUSBRODER_LIMIT || col < 2) {                            // is bad input?
            cout << "Error: input must at least 2 but below "
                << ALDOUSBRODER_LIMIT << endl;
            i = 1;                                               // set flag
        } else {
            i = 0;
        }

    } while (i);                                                // continue prompting for user input while flag is set

    maze = NULL;
    if (createMaze_AldousBroder(&maze, row, col)) {
        std::cout << "Aldous-Broder: " << std::endl;
        std::cout << *maze << std::endl;
        delete maze;
    } else {
        std::cout << "Input error";
    }

    maze = NULL;
    if (createMaze_RecursiveBacktracker(&maze, row, col)) {
        std::cout << "Recursive Backtracker: " << std::endl;
        std::cout << *maze << std::endl;
        delete maze;
    } else {
        std::cout << "Input error";
    }
    
    return 0;

}