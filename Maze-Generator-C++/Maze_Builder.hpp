/**
    * @file Maze_Builder.hpp
    * @brief functions for generating perfect Mazes
    *
    * Each function here is an algorithm for generating a perfect Maze.  A
    * perfect Maze is a Maze with only one solution.  The createMaze function is
    * used by the Host to generate the desired Maze, which is either the best
    * algorithm or a random algorithm.
*/

#ifndef MAZE_BUILDER_HPP_
#define MAZE_BUILDER_HPP_

#include "Maze.hpp"

#define ALDOUSBRODER_LIMIT 40           // maximum # of rows or columns
#define RECURSIVEBACKTRACKER_LIMIT 60
#define RECURSIVEDIVISION_LIMIT 60

class Area {
public:
    Cell *topLeft;
    unsigned int width;
    unsigned int height;
};

/**
    * @brief builds a maze using the Aldous-Broder Algorithm
    * @see http://weblog.jamisbuck.org/2011/1/17/maze-generation-aldous-broder-algorithm
    * @param[in, out] "Maze **maze" a pointer to pointer to a Maze (must be NULL). Filled in *maze will be returned.
    * @param[in] "const unsigned int numRows" # of rows
    * @param[in] "const unsigned int numCols" # of columns
    * @return true if successful
*/
bool createMaze_AldousBroder(Maze **maze, const unsigned int numRows, const unsigned int numCols);

/**
    * @brief builds a maze using the Recursive-Backtracking Algorithm
    * @see http://weblog.jamisbuck.org/2010/12/27/maze-generation-recursive-backtracking
    * @param[in, out] "Maze **maze" a pointer to pointer to a Maze (must be NULL). Filled in *maze will be returned.
    * @param[in] "const unsigned int numRows" # of rows
    * @param[in] "const unsigned int numCols" # of columns
    * @return true if successful
*/
bool createMaze_RecursiveBacktracker(Maze **maze, const unsigned int numRows, const unsigned int numCols);

/**
    * @brief builds a maze using one of the above algorithms
    * @param[in, out] "Maze **maze" a pointer to pointer to a Maze (must be NULL). Filled in *maze will be returned.
    * @param[in] "const unsigned int numRows" # of rows
    * @param[in] "const unsigned int numCols" # of columns
    * @return true if successful
*/
bool createMaze(Maze **maze, const unsigned int numRows, const unsigned int numCols);

/**
    * @brief builds a maze using the Recursive Division Algorithm
    * @see http://weblog.jamisbuck.org/2011/1/12/maze-generation-recursive-division-algorithm
    * @param[in, out] "Maze **maze" a pointer to pointer to a Maze (must be NULL). Filled in *maze will be returned.
    * @param[in] "const unsigned int numRows" # of rows
    * @param[in] "const unsigned int numCols" # of columns
    * @return true if successful
*/
bool createMaze_RecursiveDivsion(Maze **maze, const unsigned int numRows, const unsigned int numCols);

#endif
