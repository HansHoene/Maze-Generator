# maze generator module
MAZE_DIRECTORY = ./Maze-Generator-C++
MAZE_SRC_FILES = Maze.cpp Maze_Builder.cpp
MAZE_OBJECTS = $(MAZE_SRC_FILES:%.cpp=$(MAZE_DIRECTORY)/%.o)
MAZE_LIB = maze
MAZE_LIB_NAME = lib$(MAZE_LIB).dll

# host module
HOST_DIRECTORY = ./Host-C++
HOST_SRC_FILES = host.cpp IPC.cpp main.cpp Connection.cpp
HOST_OBJECTS = $(HOST_SRC_FILES:%.cpp=$(HOST_DIRECTORY)/%.o)
HOST_EXE_NAME = host.exe
HOST_SOCKET_LIB = 

# client module
CLIENT_DIRECTORY = ./Client-JAVA
CLIENT_SRC_FILES = CellStatus.java COMMAND.java Direction.java HostMessage.java IPC.java Main.java Grid.java Client.java NumberField.java
CLIENT_OBJECTS = $(CLIENT_SRC_FILES:%.java=$(CLIENT_DIRECTORY)/%.class)
CLIENT_EXE_NAME = client.jar

# C++ compiler options
CC = g++
CFLAGS = -Wall

# JAVA compiler options
JC = javac
JR = jar
JFLAGS = -sourcepath $(CLIENT_DIRECTORY) -classpath $(CLIENT_DIRECTORY)

# determine host socket library
ifeq ($(OS),Windows_NT)
	CFLAGS += -D WINDOWS
	HOST_SOCKET_LIB += -lws2_32
else
	CFLAGS += -D LINUX
	HOST_SOCKET_LIB += -lsocket
endif

.PHONY: all

all: $(HOST_EXE_NAME) $(CLIENT_EXE_NAME)

# compile host executable
$(HOST_EXE_NAME): $(HOST_OBJECTS) $(MAZE_LIB_NAME)
	$(CC) $(CFLAGS) -L . $(HOST_OBJECTS) -o $(HOST_EXE_NAME) -l$(MAZE_LIB) $(HOST_SOCKET_LIB)

#compile maze library
$(MAZE_LIB_NAME): $(MAZE_OBJECTS)
	$(CC) -shared $(CFLAGS) $(MAZE_OBJECTS) -o $(MAZE_LIB_NAME)

# compile client executable
$(CLIENT_EXE_NAME): $(CLIENT_OBJECTS)
	$(JR) cvfm $(CLIENT_EXE_NAME) $(CLIENT_DIRECTORY)/manifest.txt -C $(CLIENT_DIRECTORY) .

# compile host objects and maze objects
$(HOST_OBJECTS) $(MAZE_OBJECTS): %.o: %.cpp
	$(CC) $(CFLAGS) -I $(MAZE_DIRECTORY)/ -I $(HOST_DIRECTORY)/ -c $< -o $@

# compile client objects
$(CLIENT_OBJECTS): %.class: %.java
	$(JC) $(JFLAGS) $<

# clean build files
clean:
	$(RM) $(MAZE_OBJECTS)
	$(RM) $(HOST_OBJECTS)
	$(RM) $(CLIENT_OBJECTS)
	$(RM) $(CLIENT_DIRECTORY)/*.class

delete: clean
	$(RM) $(MAZE_LIB_NAME)
	$(RM) $(HOST_EXE_NAME)
	$(RM) $(CLIENT_EXE_NAME)
