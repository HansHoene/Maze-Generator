Build a launcher.  Make host.exe a dynamic library (dll) instead.  No login required for launcher.  Launcher only checks for updates.  If updates found, it will make changes to libhost.dll, libmaze.dll, and/or client.jar.  There can be a parsable file on the github website, which contains releases.  Every launcher has current variables (WINDOWS or LINUX).  This launcher uses variables to search for updates.

Actually, launcher might need login so that launcher knows what version it is?  Not a good idea.

Launcher can use variables to do one of 3 things
	1) send native variables to google script, which returns links to updated files or NULL
	2) interpret a script in my own language at a URL.  Launcher uses native variables to evaulate expressions in script and the script reaches either NULL for no updates required or it reaches URLs for new dlls.
	3) navigate to a URL that posts the latest release #.  If current release is equal to posted most recent release, do nothing, otherwise, download data from URLs posted.  Pick the proper URLs based on defined environment variables (in this case, OS).  Further scripting may be necessary to avoid downloading unchanged files.
	
*****************************
	Example script
	
	# version 1 (this line is ignore)
	1
	"Message that tells user about updates"
	{libhost.dll, "URL for windows version", "URL for Linux version"}
	{libmaze.dll, "URL for windows version", "URL for Linux version"}
	{client.jar, "URL for windows version", "URL for Linux version"}
	{startup.exe, }
	
	# new line above indicates next version in script
	# version 2
	2
	"New maze added"
	{libmaze.dll, "URL for windows version", "URL for Linux version"}
	# libmaze.dll is the only file listed because it is the only file that needs to be changed
	
	[empty line]
	[version #]
	[string telling user about updates]
	[file names and URLS in JSON-like format with a URL to download for Windows and a URL to download for Linux]
	...
	# this is an ignored line --  NOT AN EMPTY LINE

Launcher loads and evaluates script.  Launcher looks for file with version #.
If version # not found, download dll for each file that is most recent.  Keep
a list of URLs to download going down chain.  Otherwise, look for newer
versions.  Maybe add delete option in updates so tha updates can delete as well
as add files.
*******************************

To make game more addicting...

1) add rewards
	ex. give an # of coins based on speed and maze complexity
	ex. give objectives
		complete maze in certain amount of time
		complete maze of certain size
		complete maze that takes a certain # of moves
		complete maze without any wrong moves
	ex. types of users: master beginner expert whatever
		
	types of rewards
	The Tribe: Acceptance, recognition, sex.
	The Self: Mastery, competency, completion.
	The Hunt: Food, money, information.
	
	make rewards intermittent - not regular
	
	reward gradual mastery with bigger and biger prizes, but keep game easy to play
	
2) give user more control
	ex. choose type of maze
	ex. modes
		free play mode
		timed mode
		competition mode
		objectives mode
	
3) include an enemy
	ex. two users get same maze at same time
	or maybe enemy is the clock or the computer?
	maybe enemy is yourself. beat your own record
	
4) leave tasks unfinished so users see what they should have to accomplish

5) push notifications

6) share on social media

7) paid retries
	ex. if user fails on a time limit, use coins to buy more time

8) small goals and big goals

9) enticing colours

10) encourage repeat behaviour

11) blend adds into the visual flow of the game (on the panel!)

12) let users build a collection (of coins?)

13) add a premium version

14) take advantage of Fear Of Missing Out (FOMO)
	ex. clash of clans continues even when user is not playing
	so... maybe have user high scores
	
sources: 
https://www.sellmyapp.com/the-art-of-designing-addictive-apps-26-tips-to-get-them-hooked/
https://support.appbot.co/help-docs/5-ways-make-mobile-app-addictive/
